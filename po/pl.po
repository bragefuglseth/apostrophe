# Polish translation for apostrophe.
# Copyright © 2021-2023 the apostrophe authors.
# This file is distributed under the same license as the apostrophe package.
# Piotr Drąg <piotrdrag@gmail.com>, 2021-2023.
# Aviary.pl <community-poland@mozilla.org>, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: apostrophe\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/apostrophe/issues\n"
"POT-Creation-Date: 2023-09-11 15:13+0000\n"
"PO-Revision-Date: 2023-09-16 16:40+0200\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:3
#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:5
msgid "Apostrophe"
msgstr "Apostrof"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! Do NOT translate "uberwriter" or "apostrophe"! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:5
msgid "uberwriter;UberWriter;apostrophe;markdown;editor;text;write;"
msgstr ""
"uberwriter;UberWriter;apostrophe;markdown;edytor;tekstu;text;editor;zwykły "
"tekst;pisanie;pisz;napisz;write;"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:6
msgid "Apostrophe, an elegant, distraction-free markdown editor"
msgstr "Apostrof, elegancki, nierozpraszający edytor formatu Markdown"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:29
msgid "Color scheme"
msgstr "Schemat kolorów"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:30
msgid "Use the color scheme in the application's UI and in the text area."
msgstr "Użycie schematu kolorów w interfejsie programu i obszarze tekstu."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:36
#: data/ui/Preferences.ui:15 data/ui/Preferences.ui:16
msgid "Check spelling while typing"
msgstr "Sprawdzanie pisowni podczas pisania"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:37
msgid "Enable or disable spellchecking."
msgstr "Włącza sprawdzanie pisowni."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:43
msgid "Synchronize editor/preview scrolling"
msgstr "Synchronizacja przewijania edytora/podglądu"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:44
msgid "Keep the editor and preview scroll positions in sync."
msgstr "Synchronizuje pozycje przewijania edytora i podglądu."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:50
#: data/ui/Preferences.ui:70
msgid "Input format"
msgstr "Format wejściowy"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:51
msgid "Input format to use when previewing and exporting using Pandoc."
msgstr ""
"Format wejściowy używany podczas tworzenia podglądu i eksportowania za "
"pomocą Pandoc."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:57
msgid "Autohide Headerbar"
msgstr "Automatyczne ukrywanie paska nagłówka"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:58
msgid "Hide the header and status bars when typing."
msgstr "Ukrywa paski nagłówka i stanu podczas pisania."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:64
msgid "Open file base path"
msgstr "Podstawowa ścieżka otwierania plików"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:65
msgid "Open file paths of the current session"
msgstr "Ścieżki otwierania plików obecnej sesji"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:71
msgid "Default statistic"
msgstr "Domyślna statystyka"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:72
msgid "Which statistic is shown on the main window."
msgstr "Która statystyka jest wyświetlana w głównym oknie."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:78
msgid "Characters per line"
msgstr "Znaki na wiersz"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:79
msgid "Maximum number of characters per line within the editor."
msgstr "Maksymalna liczba znaków na wiersz w edytorze."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:85
#: data/ui/AboutHemingway.ui:6
msgid "Hemingway Mode"
msgstr "Tryb Hemingwaya"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:86
msgid "Whether the user can delete text or not."
msgstr "Czy użytkownik może usuwać tekst."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:92
msgid "Hemingway Toast Count"
msgstr "Licznik powiadomienia trybu Hemingwaya"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:93
msgid "Number of times the Hemingway Toast has been shown"
msgstr "Ile razy wyświetlono powiadomienie o trybie Hemingwaya"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:99
msgid "Preview mode"
msgstr "Tryb podglądu"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:100
msgid "How to display the preview."
msgstr "Jak wyświetlać podgląd."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:106
msgid "Preview active"
msgstr "Aktywny podgląd"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:107
msgid "Whether showing of preview is active when launching a new window."
msgstr "Czy wyświetlać podgląd po uruchomieniu nowego okna."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:113
msgid "Text size"
msgstr "Rozmiar tekstu"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:114
msgid "Preferred relative size for the text."
msgstr "Preferowany względny rozmiar tekstu."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:120
msgid "Toolbar"
msgstr "Pasek narzędziowy"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:121
msgid "Whether the toolbar is shown or not."
msgstr "Czy wyświetlać pasek narzędziowy."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:6
msgid "Edit Markdown in style"
msgstr "Elegancki edytor Markdown"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:8
msgid "Focus on your writing with a clean, distraction-free markdown editor."
msgstr ""
"Skup się na pisaniu dzięki prostemu, nierozpraszającemu edytorowi formatu "
"Markdown."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:9
msgid "Features:"
msgstr "Funkcje:"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:11
msgid "An UI tailored to comfortable writing"
msgstr "Interfejs przygotowany do wygodnego pisania"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:12
msgid "A distraction-free mode"
msgstr "Tryb bez rozproszeń"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:13
msgid "Dark, light and sepia themes"
msgstr "Motyw ciemny, jasny i w kolorach sepii"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:14
msgid ""
"Everything you expect from a text editor, such as spellchecking or document "
"statistics"
msgstr ""
"Wszystkie funkcje edytora tekstu, takie jak sprawdzanie pisowni i statystyki "
"dokumentu"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:15
msgid "Live preview of what you write"
msgstr "Podgląd pisanego tekstu na żywo"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:16
msgid ""
"Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML "
"slideshows"
msgstr ""
"Eksport do wszelkich rodzajów formatów: PDF, Word/LibreOffice, LaTeX czy "
"nawet pokazu slajdów HTML"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:41
msgid "Manuel G., Wolf V."
msgstr "Manuel G., Wolf V."

#: data/ui/About.ui.in:9
msgid "Copyright (C) 2022, Manuel G., Wolf V."
msgstr "Copyright © 2022 Manuel G., Wolf V."

#. Put your name in here, like this:
#. Manuel Genovés <manuel.genoves@gmail.com>
#: data/ui/About.ui.in:17
msgid "translator-credits"
msgstr ""
"Piotr Drąg <piotrdrag@gmail.com>, 2021-2023\n"
"Aviary.pl <community-poland@mozilla.org>, 2021-2023"

#: data/ui/AboutHemingway.ui:7
msgid ""
"The Hemingway Mode mimics the experience of writing on a typewriter, not "
"allowing you to delete any text or doing any kind of editing. \n"
"\n"
"Only being able to move forward may be frustrating, but can be an "
"interesting exercise for improving your writing skills, gaining focus or "
"facing the task on hand with a different mindset.\n"
"  "
msgstr ""
"Tryb Hemingwaya imituje pisanie na maszynie, nie pozwalając usuwać tekstu "
"ani w żaden sposób go redagować. \n"
"\n"
"Z jednej strony może to być frustrujące, z drugiej może to być ciekawe "
"ćwiczenie umiejętności pisarskich, pomoc w skupieniu uwagi lub umożliwić "
"podejście do bieżącego zadania z innej strony.\n"
"  "

#: data/ui/AboutHemingway.ui:13
msgid "_Close"
msgstr "Za_mknij"

#: data/ui/Export.ui:33 apostrophe/export_dialog.py:132
#: apostrophe/export_dialog.py:344 apostrophe/export_dialog.py:349
#: apostrophe/main_window.py:444 apostrophe/main_window.py:517
#: apostrophe/main_window.py:609
msgid "Cancel"
msgstr "Anuluj"

#: data/ui/Export.ui:44 apostrophe/export_dialog.py:127
#: apostrophe/export_dialog.py:343 apostrophe/export_dialog.py:347
msgid "Export"
msgstr "Wyeksportuj"

#: data/ui/Export.ui:114
msgid "Options"
msgstr "Opcje"

#: data/ui/Export.ui:118
msgid "Standalone"
msgstr "Samodzielny"

#: data/ui/Export.ui:120
msgid ""
"Use a header and footer to include things like stylesheets and meta "
"information"
msgstr ""
"Użycie nagłówka i stopki do dołączenia takich rzeczy, jak arkusze stylów "
"i metainformacje"

#: data/ui/Export.ui:133
msgid "Table of Contents"
msgstr "Spis treści"

#: data/ui/Export.ui:147
msgid "Number Sections"
msgstr "Numerowanie sekcji"

#: data/ui/Export.ui:165 data/ui/Export.ui:170 data/ui/Export.ui:182
#: data/ui/Export.ui:187
msgid "Page Size"
msgstr "Rozmiar strony"

#: data/ui/Export.ui:199
msgid "HTML Options"
msgstr "Opcje HTML"

#: data/ui/Export.ui:204
msgid "Self-contained"
msgstr "Niezależny"

#: data/ui/Export.ui:206
msgid "Produces an HTML file with no external dependencies"
msgstr "Tworzy plik HTML bez zewnętrznych zależności"

#: data/ui/Export.ui:222
msgid "Syntax Highlighting"
msgstr "Wyróżnianie elementów składni"

#: data/ui/Export.ui:228
msgid "Use Syntax Highlighting"
msgstr "Włączone"

#: data/ui/Export.ui:235
msgid "Highlight style"
msgstr "Styl wyróżniania"

#: data/ui/Export.ui:249
msgid "Presentation"
msgstr "Prezentacja"

#: data/ui/Export.ui:254
msgid "Incremental bullets"
msgstr "Rosnące wypunktowanie"

#: data/ui/Export.ui:256
msgid "Show one bullet point after another in a slideshow"
msgstr "Pokazuje jeden punkt po drugim w pokazie slajdów"

#: data/ui/Headerbar.ui:11
msgid "New"
msgstr "Nowy"

#: data/ui/Headerbar.ui:18
msgid "_Open"
msgstr "_Otwórz"

#: data/ui/Headerbar.ui:20
msgid "Open a markdown file"
msgstr "Otwiera plik Markdown"

#: data/ui/Headerbar.ui:26
msgid "_Save"
msgstr "_Zapisz"

#: data/ui/Headerbar.ui:28
msgid "Save the markdown file"
msgstr "Zapisuje plik Markdown"

#: data/ui/Headerbar.ui:44
msgid "Open the Main Menu"
msgstr "Otwiera menu główne"

#: data/ui/Headerbar.ui:55
msgid "_Find"
msgstr "_Wyszukiwanie"

#: data/ui/Headerbar.ui:57
msgid "Search in the File"
msgstr "Wyszukuje w pliku"

#: data/ui/Headerbar.ui:80
msgid "_Hemingway Mode"
msgstr "Tryb _Hemingwaya"

#: data/ui/Headerbar.ui:84
msgid "_Focus Mode"
msgstr "_Tryb skupienia"

#: data/ui/Headerbar.ui:90
msgid "Find and _Replace"
msgstr "Z_najdź i zastąp"

#: data/ui/Headerbar.ui:96
msgid "_Preferences"
msgstr "_Preferencje"

#: data/ui/Headerbar.ui:102
msgid "Open _Tutorial"
msgstr "Otwórz samo_uczek"

#: data/ui/Headerbar.ui:107
msgid "_Keyboard Shortcuts"
msgstr "_Skróty klawiszowe"

#: data/ui/Headerbar.ui:111
msgid "_About Apostrophe"
msgstr "_O programie"

#: data/ui/Headerbar.ui:122
msgid "_Save As..."
msgstr "Z_apisz jako…"

#: data/ui/Headerbar.ui:145
msgid "Advanced _Export…"
msgstr "Zaawansowany _eksport…"

#: data/ui/Headerbar.ui:151
msgid "_Copy HTML"
msgstr "S_kopiuj HTML"

#: data/ui/Preferences.ui:28
msgid "Autohide headerbar"
msgstr "Automatyczne ukrywanie paska nagłówka"

#: data/ui/Preferences.ui:29
msgid "Autohide header and statusbars while typing"
msgstr "Automatyczne ukrywanie pasków nagłówka i stanu podczas pisania"

#: data/ui/Preferences.ui:42
msgid "Use bigger text"
msgstr "Większy tekst"

#: data/ui/Preferences.ui:43
msgid "Reduce the margins width and increase the text size when possible"
msgstr ""
"Zmniejszenie szerokości marginesów i zwiększenie rozmiaru tekstu, kiedy to "
"możliwe"

#: data/ui/Preferences.ui:56
msgid "Restore session"
msgstr "Przywracanie sesji"

#: data/ui/Preferences.ui:57
msgid "Return to your previous session when Apostrophe is started"
msgstr "Powrót do poprzedniej sesji po uruchomieniu programu"

#: data/ui/Preferences.ui:71
msgid "Flavor of markdown Apostrophe will use"
msgstr "Używany rodzaj formatu Markdown"

#: data/ui/Preferences.ui:80
msgid "Markdown flavor documentation"
msgstr "Dokumentacja rodzaju formatu Markdown"

#: data/ui/PreviewLayoutSwitcher.ui:7 apostrophe/preview_renderer.py:208
msgid "Preview"
msgstr "Podgląd"

#: data/ui/PreviewLayoutSwitcher.ui:24
msgid "Screen Layout"
msgstr "Układ ekranu"

#: data/ui/Recents.ui:46
msgid "No Recent Documents"
msgstr "Brak ostatnio używanych dokumentów"

#: data/ui/Recents.ui:69
msgid "No Results Found"
msgstr "Brak wyników"

#: data/ui/SearchBar.ui:28
msgid "Previous Match"
msgstr "Poprzedni wynik"

#: data/ui/SearchBar.ui:37
msgid "Next Match"
msgstr "Następny wynik"

#. Translators: This indicates case sensitivity, so it should be the first vowel of the language in lowercase and uppercase
#: data/ui/SearchBar.ui:52
msgid "aA"
msgstr "aA"

#: data/ui/SearchBar.ui:55
msgid "Case Sensitive"
msgstr "Rozróżnianie małych i wielkich liter"

#: data/ui/SearchBar.ui:64
msgid "Regular Expression"
msgstr "Wyrażenie regularne"

#: data/ui/SearchBar.ui:72 data/ui/SearchBar.ui:110
msgid "Replace"
msgstr "Zastąp"

#: data/ui/SearchBar.ui:118
msgid "Replace All"
msgstr "Zastąp wszystko"

#: data/ui/Shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "Ogólne"

#: data/ui/Shortcuts.ui:14
msgctxt "shortcut window"
msgid "New"
msgstr "Nowy"

#: data/ui/Shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open"
msgstr "Otwieranie"

#: data/ui/Shortcuts.ui:26
msgctxt "shortcut window"
msgid "Save"
msgstr "Zapisanie"

#: data/ui/Shortcuts.ui:32
msgctxt "shortcut window"
msgid "Save as"
msgstr "Zapisanie jako"

#: data/ui/Shortcuts.ui:38
msgctxt "shortcut window"
msgid "Close document"
msgstr "Zamknięcie dokumentu"

#: data/ui/Shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show preferences"
msgstr "Wyświetlenie preferencji"

#: data/ui/Shortcuts.ui:50
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "Skróty klawiszowe"

#: data/ui/Shortcuts.ui:56
msgctxt "shortcut window"
msgid "Quit application"
msgstr "Zakończenie działania programu"

#: data/ui/Shortcuts.ui:64
msgctxt "shortcut window"
msgid "Modes"
msgstr "Tryby"

#: data/ui/Shortcuts.ui:67
msgctxt "shortcut window"
msgid "Focus mode"
msgstr "Tryb skupienia"

#: data/ui/Shortcuts.ui:73
msgctxt "shortcut window"
msgid "Hemingway mode"
msgstr "Tryb Hemingwaya"

#: data/ui/Shortcuts.ui:80
msgctxt "shortcut window"
msgid "Preview"
msgstr "Podgląd"

#: data/ui/Shortcuts.ui:86
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Pełny ekran"

#: data/ui/Shortcuts.ui:94 data/ui/Shortcuts.ui:97
msgctxt "shortcut window"
msgid "Find"
msgstr "Wyszukiwanie"

#: data/ui/Shortcuts.ui:103
msgctxt "shortcut window"
msgid "Find and replace"
msgstr "Wyszukiwanie i zastępowanie"

#: data/ui/Shortcuts.ui:111
msgctxt "shortcut window"
msgid "Markdown"
msgstr "Markdown"

#: data/ui/Shortcuts.ui:114
msgctxt "shortcut window"
msgid "Separator"
msgstr "Separator"

#: data/ui/Shortcuts.ui:120
msgctxt "shortcut window"
msgid "Header"
msgstr "Nagłówek"

#: data/ui/Shortcuts.ui:126
msgctxt "shortcut window"
msgid "List item"
msgstr "Element listy"

#: data/ui/Shortcuts.ui:133
msgctxt "shortcut window"
msgid "Italic"
msgstr "Pochylenie"

#: data/ui/Shortcuts.ui:139
msgctxt "shortcut window"
msgid "Bold"
msgstr "Pogrubienie"

#: data/ui/Shortcuts.ui:145
msgctxt "shortcut window"
msgid "Strikeout"
msgstr "Przekreślenie"

#: data/ui/Shortcuts.ui:153
msgctxt "shortcut window"
msgid "Copy and paste"
msgstr "Kopiowanie i wklejanie"

#: data/ui/Shortcuts.ui:156
msgctxt "shortcut window"
msgid "Copy selected text to clipboard"
msgstr "Skopiowanie zaznaczonego tekstu do schowka"

#: data/ui/Shortcuts.ui:162
msgctxt "shortcut window"
msgid "Cut selected text to clipboard"
msgstr "Wycięcie zaznaczonego tekstu do schowka"

#: data/ui/Shortcuts.ui:168
msgctxt "shortcut window"
msgid "Paste selected text from clipboard"
msgstr "Wklejenie zaznaczonego tekstu ze schowka"

#: data/ui/Shortcuts.ui:176
msgctxt "shortcut window"
msgid "Undo and redo"
msgstr "Cofanie i ponawianie"

#: data/ui/Shortcuts.ui:179
msgctxt "shortcut window"
msgid "Undo previous command"
msgstr "Cofnięcie poprzedniego polecenia"

#: data/ui/Shortcuts.ui:185
msgctxt "shortcut window"
msgid "Redo previous command"
msgstr "Ponowienie poprzedniego polecenia"

#: data/ui/Shortcuts.ui:193
msgctxt "shortcut window"
msgid "Selection"
msgstr "Zaznaczanie"

#: data/ui/Shortcuts.ui:196
msgctxt "shortcut window"
msgid "Select all text"
msgstr "Zaznaczenie całego tekstu"

#: data/ui/TexliveWarning.ui:25 data/ui/TexliveWarning.ui:79
msgid "TexLive Required"
msgstr "Wymagane jest rozszerzenie TexLive"

#: data/ui/TexliveWarning.ui:33
msgid ""
"Apostrophe needs the TeXLive extension\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from Apostrophe's page in Software\n"
"or by running the following command in a terminal:"
msgstr ""
"Ten program wymaga rozszerzenia TeXLive,\n"
"aby eksportować pliki PDF i LaTeX.\n"
"\n"
"Proszę je zainstalować ze strony programu w Menedżerze oprogramowania\n"
"lub wykonując to polecenie w terminalu:"

#: data/ui/TexliveWarning.ui:53
msgid "Copy to clipboard"
msgstr "Skopiuj do schowka"

#: data/ui/TexliveWarning.ui:87
msgid ""
"Apostrophe needs TeXLive\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from your distribution repositories"
msgstr ""
"Ten program wymaga rozszerzenia TeXLive,\n"
"aby eksportować pliki PDF i LaTeX.\n"
"\n"
"Proszę je zainstalować z repozytoriów używanej dystrybucji"

#: data/ui/ThemeSwitcher.ui:17
msgid "Use System Colors"
msgstr "Kolory systemowe"

#: data/ui/ThemeSwitcher.ui:27
msgid "Use Light Colors"
msgstr "Jasne kolory"

#: data/ui/ThemeSwitcher.ui:37
msgid "Use Sepia Colors"
msgstr "Kolory sepii"

#: data/ui/ThemeSwitcher.ui:47
msgid "Use Dark Colors"
msgstr "Ciemne kolory"

#: data/ui/Window.ui:86
msgid "File Has Changed on Disk"
msgstr "Plik na dysku został zmieniony"

#: data/ui/Window.ui:96
msgid "The file has been changed by another program."
msgstr "Plik został zmieniony przez inny program."

#: data/ui/Window.ui:104
msgid "_Discard Changes and Reload"
msgstr "_Odrzuć zmiany i wczytaj ponownie"

#: apostrophe/application.py:232
msgid "Donate"
msgstr "Przekaż datek"

#: apostrophe/application.py:233
msgid "Translations"
msgstr "Tłumaczenia"

#: apostrophe/export_dialog.py:122 apostrophe/helpers.py:84
msgid "Close"
msgstr "Zamknij"

#: apostrophe/export_dialog.py:130 apostrophe/export_dialog.py:348
#, python-format
msgid "Export to %s"
msgstr "Wyeksportuj do %s"

#: apostrophe/export_dialog.py:156 apostrophe/export_dialog.py:337
#, python-brace-format
msgid ""
"An error happened while trying to export:\n"
"\n"
"{err_msg}"
msgstr ""
"Wystąpił błąd podczas próby eksportu:\n"
"\n"
"{err_msg}"

#: apostrophe/export_dialog.py:235
msgid "Export to {}"
msgstr "Wyeksportuj do {}"

#: apostrophe/export_dialog.py:344
msgid "Select folder"
msgstr "Wybierz katalog"

#: apostrophe/helpers.py:83
msgid "Error"
msgstr "Błąd"

#: apostrophe/inhibitor.py:36
msgid "Unsaved documents"
msgstr "Niezapisane dokumenty"

#: apostrophe/inline_preview.py:201
msgid "Formula looks incorrect:"
msgstr "Wzór jest niepoprawny:"

#: apostrophe/inline_preview.py:241
msgid "No matching footnote found"
msgstr "Nie odnaleziono pasującego przypisu"

#: apostrophe/inline_preview.py:261
msgid "noun"
msgstr "rzeczownik"

#: apostrophe/inline_preview.py:263
msgid "verb"
msgstr "czasownik"

#: apostrophe/inline_preview.py:265
msgid "adjective"
msgstr "przymiotnik"

#: apostrophe/inline_preview.py:267
msgid "adverb"
msgstr "przysłówek"

#. Hemingway Toast
#: apostrophe/main_window.py:128
msgid "Text can't be deleted while on Hemingway mode"
msgstr "W trybie Hemingwaya nie można usuwać tekstu"

#: apostrophe/main_window.py:131
msgid "Tell me more"
msgstr "Więcej informacji"

#: apostrophe/main_window.py:440
msgid "Save your File"
msgstr "Zapis pliku"

#: apostrophe/main_window.py:443 apostrophe/main_window.py:611
msgid "Save"
msgstr "Zapisz"

#: apostrophe/main_window.py:506
msgid "Markdown Files"
msgstr "Pliki Markdown"

#: apostrophe/main_window.py:510
msgid "Plain Text Files"
msgstr "Zwykły tekst"

#: apostrophe/main_window.py:513
msgid "Open a .md file"
msgstr "Otwarcie pliku .md"

#: apostrophe/main_window.py:516
msgid "Open"
msgstr "Otwórz"

#: apostrophe/main_window.py:603
msgid "Save changes?"
msgstr "Zapisać zmiany?"

#: apostrophe/main_window.py:604
#, python-format
msgid ""
"“%s” contains unsaved changes. If you don’t save, all your changes will be "
"permanently lost."
msgstr ""
"Dokument „%s” zawiera niezapisane zmiany. Jeżeli dokument nie zostanie "
"zapisany, wszystkie zmiany zostaną bezpowrotnie utracone."

#: apostrophe/main_window.py:610
msgid "Discard"
msgstr "Odrzuć"

#: apostrophe/main_window.py:768 apostrophe/main_window.py:792
msgid "New File"
msgstr "Nowy plik"

#: apostrophe/preview_layout_switcher.py:40
msgid "Full-Width"
msgstr "Pełna szerokość"

#: apostrophe/preview_layout_switcher.py:42
msgid "Half-Width"
msgstr "Podział pionowy"

#: apostrophe/preview_layout_switcher.py:44
msgid "Half-Height"
msgstr "Podział poziomy"

#: apostrophe/preview_layout_switcher.py:46
msgid "Windowed"
msgstr "W oknie"

#: apostrophe/stats_handler.py:98
msgid "{:n} of {:n} Characters"
msgstr "{:n} z {:n} znaków"

#: apostrophe/stats_handler.py:100
msgid "{:n} Character"
msgid_plural "{:n} Characters"
msgstr[0] "{:n} znak"
msgstr[1] "{:n} znaki"
msgstr[2] "{:n} znaków"

#: apostrophe/stats_handler.py:103
msgid "{:n} of {:n} Words"
msgstr "{:n} z {:n} słów"

#: apostrophe/stats_handler.py:105
msgid "{:n} Word"
msgid_plural "{:n} Words"
msgstr[0] "{:n} słowo"
msgstr[1] "{:n} słowa"
msgstr[2] "{:n} słów"

#: apostrophe/stats_handler.py:108
msgid "{:n} of {:n} Sentences"
msgstr "{:n} z {:n} zdań"

#: apostrophe/stats_handler.py:110
msgid "{:n} Sentence"
msgid_plural "{:n} Sentences"
msgstr[0] "{:n} zdanie"
msgstr[1] "{:n} zdania"
msgstr[2] "{:n} zdań"

#: apostrophe/stats_handler.py:113
msgid "{:n} of {:n} Paragraphs"
msgstr "{:n} z {:n} akapitów"

#: apostrophe/stats_handler.py:115
msgid "{:n} Paragraph"
msgid_plural "{:n} Paragraphs"
msgstr[0] "{:n} akapit"
msgstr[1] "{:n} akapity"
msgstr[2] "{:n} akapitów"

#: apostrophe/stats_handler.py:118
msgid "{:d}:{:02d}:{:02d} of {:d}:{:02d}:{:02d} Read Time"
msgstr "Czas czytania: {:d}:{:02d}:{:02d} z {:d}:{:02d}:{:02d}"

#: apostrophe/stats_handler.py:120
msgid "{:d}:{:02d}:{:02d} Read Time"
msgstr "Czas czytania: {:d}∶{:02d}∶{:02d}"

#: apostrophe/text_view_format_inserter.py:24
msgid "italic text"
msgstr "pochylony tekst"

#: apostrophe/text_view_format_inserter.py:29
msgid "bold text"
msgstr "pogrubiony tekst"

#: apostrophe/text_view_format_inserter.py:34
msgid "strikethrough text"
msgstr "przekreślony tekst"

#: apostrophe/text_view_format_inserter.py:49
#: apostrophe/text_view_format_inserter.py:117
#: apostrophe/text_view_format_inserter.py:187
msgid "Item"
msgstr "Element"

#: apostrophe/text_view_format_inserter.py:258
msgid "Header"
msgstr "Nagłówek"

#: apostrophe/text_view_format_inserter.py:329
msgid "Quote"
msgstr "Cytat"

#: apostrophe/text_view_format_inserter.py:371
msgid "code"
msgstr "kod"

#: apostrophe/text_view_format_inserter.py:394
msgid "https://www.example.com"
msgstr "https://www.example.com"

#: apostrophe/text_view_format_inserter.py:395
msgid "link text"
msgstr "tekst odnośnika"

#: apostrophe/text_view_format_inserter.py:429
#: apostrophe/text_view_format_inserter.py:454
msgid "image caption"
msgstr "podpis obrazu"

#: apostrophe/text_view_format_inserter.py:435
msgid "Image"
msgstr "Obraz"

#: apostrophe/text_view_format_inserter.py:441
msgid "Select an image"
msgstr "Wybór obrazu"

#: apostrophe/text_view_format_inserter.py:543
msgid ""
"\n"
"code\n"
msgstr ""
"\n"
"kod\n"

#: apostrophe/text_view.py:169 apostrophe/text_view.py:171
msgid "web page"
msgstr "strona WWW"

msgid "Show Statistics"
msgstr "Wyświetla statystyki"

msgid "0 Words"
msgstr "0 słów"
